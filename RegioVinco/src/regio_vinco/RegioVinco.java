package regio_vinco;

import java.io.File;
import java.util.Iterator;
import javafx.application.Application;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import regio_vinco_data.Region;
import regio_vinco_data.RegionDataIO;
import regio_vinco_data.RegionDataManager;

/**
 * This is the Regio Vinco game application. Note that it extends the
 * PointAndClickGame class and overrides all the proper methods for setting up
 * the Data, the GUI, the Event Handlers, and update and timer task, the thing
 * that actually does the update scheduled rendering.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class RegioVinco extends Application {
    
    public static RegionDataManager regionDataManager;
    
    // THESE CONSTANTS SETUP THE GAME DIMENSIONS. THE GAME WIDTH
    // AND HEIGHT SHOULD MIRROR THE BACKGROUND IMAGE DIMENSIONS. WE
    // WILL NOT RENDER ANYTHING OUTSIDE THOSE BOUNDS.

    public static final int GAME_WIDTH = 1200;
    public static final int GAME_HEIGHT = 700;

    // FOR THIS APP WE'RE ONLY PLAYING WITH ONE MAP, BUT
    // IN THE FUTURE OUR GAMES WILL USE LOTS OF THEM
    public static final String SLASH = "/";
    public static final String MAP_EXT = " Map.png";
    public static final String XML_EXT = " Data.xml";
    public static final String FLAG_EXT = " Flag.png";
    public static final String NATIONAL_ANTHEM_EXT = " National Anthem.mid";
    
    public static final String REGION_DATA_PATH = "The World/";
    public static final String REGION_DATA_SCHEMA = REGION_DATA_PATH + "RegionData.xsd";
    public static String CURRENT_REGION_PATH = "The World";
    public static String REGION_NAME = "The World";
    
    public static String CURRENT_PATH = REGION_NAME + SLASH;
    public static String REGION_MAP_PATH = CURRENT_PATH + REGION_NAME + " Map.png";
    public static String REGION_XML_PATH = CURRENT_PATH + REGION_NAME + " Data.xml";
    public static final String MAPS_PATH = "./data/maps/";
    public static final String AFG_MAP_FILE_PATH = MAPS_PATH + "The World Map.png";
    
    // SOME COLORS
    public static final Color MAP_COLOR_KEY = RegioVincoDataModel.makeColor(220, 110, 0);
    public static final Color MAP_COLOR_BLACK = RegioVincoDataModel.makeColor(0, 0, 0);
    public static final Color TRANSPARENT_COLOR = Color.color(0, 0, 0, 0);
    
    // HERE ARE THE PATHS TO THE REST OF THE IMAGES WE'LL USE
    public static final String GUI_PATH = "./data/gui/";
    public static final String SPLASH_FILE_PATH = GUI_PATH + "RegioVincoSplash.jpg";
    public static final String ENTER_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoEnterButton.png";
    public static final String ENTER_BUTTON_MO_FILE_PATH = GUI_PATH + "RegioVincoEnterButtonMouseOver.png";
    public static final String HOME_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoHomeButton.png";
    public static final String HOME_DISABLED_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoHomeButtonDisabled.png";
    public static final String SETTING_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoSettingButton.png";
    public static final String SETTING_DISABLED_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoSettingButtonDisabled.png";
    public static final String HELP_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoHelpButton.png";
    public static final String HELP_DISABLED_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoHelpButtonDisabled.png";
    public static final String NAME_GAME_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoNameButton.png";
    public static final String NAME_GAME_DISABLED_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoNameButtonDisabled.png";
    public static final String CAPITAL_GAME_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoCapitalButton.png";
    public static final String CAPITAL_GAME_DISABLED_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoCapitalButtonDisabled.png";
    public static final String LEADER_GAME_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoLeaderButton.png";
    public static final String LEADER_GAME_DISABLED_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoLeaderButtonDisabled.png";
    public static final String FLAG_GAME_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoFlagButton.png";
    public static final String FLAG_GAME_DISABLED_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoFlagButtonDisabled.png";
    public static final String FLAG_NOT_FOUND_FILE_PATH = GUI_PATH + "RegioVincoNoFlag.png";
    public static final String STOP_GAME_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoStopButton.png";
    public static final String STOP_GAME_DISABLED_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoStopButtonDisabled.png";
    public static final String BACKGROUND_FILE_PATH = GUI_PATH + "RegioVincoBackground.jpg";
    public static final String SETTING_FILE_PATH = GUI_PATH + "RegioVincoSettingPane.jpg";
    public static final String HELP_FILE_PATH = GUI_PATH + "RegioVincoHelpPane.jpg";
    public static final String TITLE_FILE_PATH = GUI_PATH + "RegioVincoTitle.png";
    public static final String START_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoStartButton.png";
    public static final String START_BUTTON_MO_FILE_PATH = GUI_PATH + "RegioVincoStartButtonMouseOver.png";
    public static final String EXIT_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoExitButton.png";
    public static final String EXIT_BUTTON_MO_FILE_PATH = GUI_PATH + "RegioVincoExitButtonMouseOver.png";
    public static final String SUB_REGION_FILE_PATH = GUI_PATH + "RegioVincoSubRegion.png";
    public static final String CLOSE_WIN_DISPLAY_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoWinDisplayClose.png";
    public static final String WIN_DISPLAY_FILE_PATH = GUI_PATH + "RegioVincoWinDisplayCopy.png";

    // HERE ARE SOME APP-LEVEL SETTINGS, LIKE THE FRAME RATE. ALSO,
    // WE WILL BE LOADING SpriteType DATA FROM A FILE, SO THAT FILE
    // LOCATION IS PROVIDED HERE AS WELL. NOTE THAT IT MIGHT BE A 
    // GOOD IDEA TO LOAD ALL OF THESE SETTINGS FROM A FILE, BUT ALAS,
    // THERE ARE ONLY SO MANY HOURS IN A DAY
    public static final int TARGET_FRAME_RATE = 30;
    public static final String APP_TITLE = "Regio Vinco";
    
    // SPLASH IMAGE
    public static final String SPLASH_TYPE = "SPLASH_TYPE";
    public static final int SPLASH_X = 0;
    public static final int SPLASH_Y = 0;
    
    // ENTER BUTTON
    public static final String ENTER_TYPE = "ENTER_TYPE";
    public static final int ENTER_X = 540;
    public static final int ENTER_Y = 650;
    
    // HOME BUTTON
    public static final String HOME_TYPE = "HOME_TYPE";
    public static final int HOME_X = 1110;
    public static final int HOME_Y = 70;
    
    // SETTING BUTTON
    public static final String SETTING_TYPE = "SETTING_TYPE";
    public static final int SETTING_X = 1140;
    public static final int SETTING_Y = 70;
    
    // HELP BUTTON
    public static final String HELP_TYPE = "HELP_TYPE";
    public static final int HELP_X = 1170;
    public static final int HELP_Y = 70;           
    
    // NAME GAME MODE BUTTON
    public static final String NAME_GAME_TYPE = "NAME_GAME_TYPE";
    public static final int NAME_GAME_X = 900;
    public static final int NAME_GAME_Y = 100;
    
    // CAPITAL GAME MODE BUTTON
    public static final String CAPITAL_GAME_TYPE = "CAPITAL_GAME_TYPE";
    public static final int CAPITAL_GAME_X = 960;
    public static final int CAPITAL_GAME_Y = 100;
    
    // LEADER GAME MODE BUTTON
    public static final String LEADER_GAME_TYPE = "LEADER_GAME_TYPE";
    public static final int LEADER_GAME_X = 1020;
    public static final int LEADER_GAME_Y = 100;
    
    // FLAG GAME MODE BUTTON
    public static final String FLAG_GAME_TYPE = "FLAG_GAME_TYPE";
    public static final int FLAG_GAME_X = 1080;
    public static final int FLAG_GAME_Y = 100;
    
    // STOP GAME BUTTON
    public static final String STOP_GAME_TYPE = "STOP_GAME_TYPE";
    public static final int STOP_GAME_X = 1140;
    public static final int STOP_GAME_Y = 100;
    
    // BACKGROUND IMAGE
    public static final String BACKGROUND_TYPE = "BACKGROUND_TYPE";
    public static final int BACKGROUND_X = 0;
    public static final int BACKGROUND_Y = 0;
    
    // SETTING IMAGE
    public static final String SETTING_PANE_TYPE = "SETTING_PANE_TYPE";
    public static final int SETTING_PANE_X = 0;
    public static final int SETTING_PANE_Y = 0;
    
    // HELP IMAGE
    public static final String HELP_PANE_TYPE = "HELP_PANE_TYPE";
    public static final int HELP_PANE_X = 0;
    public static final int HELP_PANE_Y = 0;
    
    // TITLE IMAGE
    public static final String TITLE_TYPE = "TITLE_TYPE";
    public static final int TITLE_X = 900;
    public static final int TITLE_Y = 0;
    
    // START GAME BUTTON
    public static final String START_TYPE = "START_TYPE";
    public static final int START_X = 900;
    public static final int START_Y = 100;

    // EXIT GAME BUTTON
    public static final String EXIT_TYPE = "EXIT_TYPE";
    public static final int EXIT_X = 1050;
    public static final int EXIT_Y = 100;
    
    // THE GAME MAP LOCATION
    public static final String MAP_TYPE = "MAP_TYPE";
    public static final String SUB_REGION_TYPE = "SUB_REGION_TYPE";
    public static final int MAP_X = 0;
    public static final int MAP_Y = 0;

    // THE WIN DIALOG
    public static final String WIN_DISPLAY_TYPE = "WIN_DISPLAY";
    public static final int WIN_X = 350;
    public static final int WIN_Y = 150;
    
    // THE WIN DIALOG FLAG
    public static final String WIN_DISPLAY_FLAG_TYPE = "WIND_DISPLAY_FLAG_TYPE";
    public static final int WIN_DISPLAY_FLAG_X = 370;
    public static final int WIN_DISPLAY_FLAG_Y = 170;
    
    // THE BUTTON TO CLOSE THE WIN DIALOG
    public static final String CLOSE_WIN_DISPLAY_TYPE = "CLOSE_WIN_DISPLAY_TYPE";
    public static final int CLOSE_WIN_DISPLAY_X = 850;
    public static final int CLOSE_WIN_DISPLAY_Y = 150;
    
    // THIS IS THE X WHERE WE'LL DRAW ALL THE STACK NODES
    public static final int STACK_X = 900;
    public static final int STACK_INIT_Y = 600;
    public static final int STACK_INIT_Y_INC = 50;

    // THE REGION SCORES
    public static final Color REGION_SCORE_COLOR = RegioVincoDataModel.makeColor(200, 160, 80);
    public static final Font REGION_SCORE_FONT = Font.font("Segoe Script", FontWeight.NORMAL, 20);
    public static final int REGION_SCORE_Y = 630;
    public static final int REGION_SCORE_X =  10;
    
    // NAVIGATIONS
    public static final Color NAVI_COLOR = RegioVincoDataModel.makeColor(200, 160, 80);
    public static final Font NAVI_FONT = Font.font("Segoe Script", FontWeight.NORMAL, 20);
    public static final int NAVI_Y = 670;
    public static final int NAVI_X = 10;
    
    // CURRENT REGION NAME ON TOP
    public static final Color REGION_NAME_COLOR = RegioVincoDataModel.makeColor(255,255,255);
    public static final Font REGION_NAME_FONT = Font.font("Segoe Script", FontWeight.NORMAL, 45);
    public static final int REGION_NAME_X = 330;
    public static final int REGION_NAME_Y = 0;
    
    // FOR THE REGION NAME ON GAME
    public static final int REGION_NAME_ON_GAME_X = 900;
    public static final int REGION_NAME_ON_GAME_Y = 160;
    public static final Color REGION_NAME_ON_GAME_COLOR = RegioVincoDataModel.makeColor(220, 220, 80);
    public static final Font REGION_NAME_ON_GAME_FONT = Font.font("Segoe Script", FontWeight.NORMAL, 32);
    public static final int REGION_NAME_ON_GAME_WIDTH = 300;
    public static final int REGION_NAME_ON_GAME_HEIGHT = 50;
    
    // POINTED SUB REGION NAME & STATS
    public static final Color POINT_SUBREGION_COLOR = RegioVincoDataModel.makeColor(200, 160, 80);
    public static final Font POINT_SUBREGION_FONT = Font.font("Segoe Script", FontWeight.NORMAL, 20);
    public static final int POINT_SUBREGION_X = 900;
    public static final int POINT_SUBREGION_Y = 200;
    
    // POINTED SUB REGION FLAG
    public static final String POINT_SUBREGION_FLAG_TYPE = "POINT_SUBREGION_FLAG_TYPE";
    public static final int POINT_SUBREGION_FLAG_X = 900;
    public static final int POINT_SUBREGION_FLAG_Y = 450;
    
    // STATS ON GAME
    public static final Color STATS_COLOR = RegioVincoDataModel.makeColor(200, 160, 80);
    public static final Font STATS_FONT = Font.font("Segoe Script", FontWeight.NORMAL, 20);
    public static final int STATS_Y = 650;
    public static final int STATS_X =  10;
    
    // FOR THE STACK
    public static final Color SUB_REGION_NAME_COLOR = RegioVincoDataModel.makeColor(10, 10, 100);
    public static final Font SUB_REGION_FONT = Font.font("Segoe Script", FontWeight.NORMAL, 26);
    public static final Color BOTTOM_SUB_REGION_FILL_COLOR = RegioVincoDataModel.makeColor(100, 220, 100);
    public static final Color BOTTOM_SUB_REGION_TEXT_COLOR = Color.RED;
    public static final Color WRONG_SUB_REGION_FILL_COLOR = RegioVincoDataModel.makeColor(240, 0, 0);
    
    // TOTAL STATS AFTER GAME WON
    public static final Color DIALOG_STATS_COLOR = RegioVincoDataModel.makeColor(5, 5, 120);
    public static final Font DIALOG_STATS_FONT = Font.font("Segoe Script", FontWeight.NORMAL, 20);
    public static final int DIALOG_STATS_X = 400;
    public static final int DIALOG_STATS_Y = 330;
    
    // CHECK BOX FOR SOUND
    public static final int SOUND_X = 385;
    public static final int SOUND_Y = 320;
    public static final Font CHECKBOX_FONT = Font.font("Segoe Script", FontWeight.NORMAL, 18);
    
    // CHECK BOX FOR MUSIC
    public static final int MUSIC_X = 385;
    public static final int MUSIC_Y = 360;
    
    // SUMMARY OF THE GAME
    public static final Color SUMMARY_COLOR = RegioVincoDataModel.makeColor(0, 0, 0);
    public static final Font SUMMARY_FONT = Font.font("Segoe Script", FontWeight.NORMAL, 18);
    public static final int SUMMARY_X = 70;
    public static final int SUMMARY_Y = 70;
    public static final int SUMMARY_WIDTH = 750;

    public static final int SUB_STACK_VELOCITY = 5;
    public static final int FIRST_REGION_Y_IN_STACK = GAME_HEIGHT-10;

    public static final String AUDIO_DIR = "./data/audio/";
    public static final String AFGHAN_ANTHEM_FILE_NAME = AUDIO_DIR + "AfghanistanNationalAnthem.mid";
    public static final String DEFAULT_ANTHEM_FILE_NAME = AUDIO_DIR + "DefaultAnthem.mid";
    public static final String SUCCESS_FILE_NAME = AUDIO_DIR + "Success.wav";
    public static final String FAILURE_FILE_NAME = AUDIO_DIR + "Failure.wav";
    public static final String TRACKED_FILE_NAME = AUDIO_DIR + "Tracked.wav";
    public static final String AFGHAN_ANTHEM = "AFGHAN_ANTHEM";
    public static final String NATIONAL_ANTHEM = "NATIONAL_ANTHEM";
    public static final String DEFAULT_ANTHEM = "DEFAULT_ANTHEM";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAILURE = "FAILURE";
    public static final String TRACKED_SONG = "TRACKED_SONG";

    public static boolean GAME_STARTED = false;
    public static boolean GAME_IN_PROGRESS = false;
    public static final Color NONPLAYABLE_REGION_COLOR = RegioVincoDataModel.makeColor(255,150,150);
    /**
     * This is where the RegioVinco application starts. It proceeds to make a
     * game and pass it the window, and then starts it.
     *
     * @param primaryStage The window for this JavaFX application.
     */
    @Override
    public void start(Stage primaryStage) {
        // MAKE THE REGION DATA MANAGER
        regionDataManager = new RegionDataManager();
        
        // INIT THE FILE I/O
        // AND OUR IMPORTER/EXPORTER
        File schemaFile = new File(REGION_DATA_SCHEMA);
        RegionDataIO regionDataIO = new RegionDataIO(schemaFile);
        regionDataManager.setRegionImporterExporter(regionDataIO);
        
	RegioVincoGame game = new RegioVincoGame(primaryStage);
	game.startGame();
    }

    /**
     * The RegioVinco game application starts here. All game data and GUI
     * initialization is done through the constructor, so we will just construct
     * our game and set it visible to start it up.
     *
     * @param args command line arguments, which will not be used
     */
    public static void main(String[] args) {
	launch(args);
        System.out.println("Game Overed!!");
    }
}
