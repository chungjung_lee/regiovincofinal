package regio_vinco;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import pacg.KeyPressHook;

/**
 * This controller provides the appropriate responses for all interactions.
 */
public class RegioVincoController implements KeyPressHook {
    RegioVincoGame game;
    
    public RegioVincoController(RegioVincoGame initGame) {
	game = initGame;
    }
    
    public void processEnterRequest() {
        try {
            game.beginUsingData();
            game.navigationMode();
        }
        finally {
            game.endUsingData();
        }
    }
    
    public void processHomeRequest() {
        game.screenChangeButtons(false, true, true);
        game.screenChanges(true, false, false, false);
        game.labelChanges(true, true, true, false, false);
        game.gameModeButtonsVisible(true);
    }
    
    public void processSettingRequest() {
        game.screenChangeButtons(true, false, true);
        game.screenChanges(false, false, true, false);
        game.labelChanges(false, false, false, false, false);
        game.gameModeButtonsVisible(false);
    }
    
    public void processHelpRequest() {
        game.screenChangeButtons(true, true, false);
        game.screenChanges(false, false, false, true);
        game.labelChanges(false, false, false, false, false);
        game.gameModeButtonsVisible(false);
    }
    
    public void processNameGameRequest() {
        try {
	    game.beginUsingData();
            game.labelChanges(true, false, false, true, true);
            game.screenChangeButtons(false, false, false);
	    game.reset();
	}
	finally {
	    game.endUsingData();
	}
    }
    
    public void processCapitalGameRequest() {
        try {
	    game.beginUsingData();
            game.labelChanges(true, false, false, true, true);
            game.screenChangeButtons(false, false, false);
	    game.reset();
	}
	finally {
	    game.endUsingData();
	}
        
    }
    
    public void processLeaderGameRequest() {
        try {
	    game.beginUsingData();
            game.labelChanges(true, false, false, true, true);
            game.screenChangeButtons(false, false, false);
	    game.reset();
	}
	finally {
	    game.endUsingData();
	}
        
    }
    
    public void processFlagGameRequest() {
        try {
	    game.beginUsingData();
            game.labelChanges(true, false, false, true, true);
            game.screenChangeButtons(false, false, false);
	    game.reset();
	}
	finally {
	    game.endUsingData();
	}
        
    }
    
    public void processStopGameRequest() {
        try {
            game.beginUsingData();
            game.stopGame();
        }
        finally {
            game.endUsingData();
            System.out.println("Stop Game Clicked");
        }
        
    }
    
    public void processStartGameRequest() {
	try {
	    game.beginUsingData();
	    game.reset();
	}
	finally {
	    game.endUsingData();
	}
    }
    
    public void processExitGameRequest() {
	game.killApplication();
    }
    
    public void processMapClickRequest(boolean gameStarted, int x, int y) {
	try {
	    game.beginUsingData();
            if(gameStarted) {
                ((RegioVincoDataModel)game.getDataModel()).respondToMapSelection(game, x, y);
            }
            else {
                ((RegioVincoDataModel)game.getDataModel()).respondToNavigation(game, x, y);
            }
	}
	finally {
	    game.endUsingData();
	}
    }
    
    public void processMapMoveRequest(int x, int y) {
        try {
            game.beginUsingData();
            ((RegioVincoDataModel)game.getDataModel()).respondToMapMove(game, x, y);
        }
        finally {
            game.endUsingData();
        }
    }
    
    public void processNaviRequest() {
        try {
            game.beginUsingData();
            ((RegioVincoDataModel)game.getDataModel()).respondToUpperNavigation(game);
        }
        finally {
            game.endUsingData();
        }
    }
    
    @Override
    public void processKeyPressHook(KeyEvent ke)
    {
        KeyCode keyCode = ke.getCode();
        if (keyCode == KeyCode.C)
        {
            try
            {    
                game.beginUsingData();
                RegioVincoDataModel dataModel = (RegioVincoDataModel)(game.getDataModel());
                dataModel.removeAllButOneFromStack(game);         
            }
            finally
            {
                game.endUsingData();
            }
        }
    }   
}
