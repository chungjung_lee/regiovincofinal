package regio_vinco;

import audio_manager.AudioManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import pacg.PointAndClickGame;
import pacg.PointAndClickGameDataModel;
import regio_vinco_data.RegionDataManager;
import static regio_vinco.RegioVinco.*;
import static regio_vinco.RegioVincoGame.GAME_MODE;
import static regio_vinco.RegioVincoGame.currentRegion;
import regio_vinco_data.Region;
import regio_vinco_data.RegionPlayable;

/**
 * This class manages the game data for the Regio Vinco game application. Note
 * that this game is built using the Point & Click Game Framework as its base. 
 * This class contains methods for managing game data and states.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class RegioVincoDataModel extends PointAndClickGameDataModel {
    // THIS IS THE MAP IMAGE THAT WE'LL USE
    private WritableImage mapImage;
    private PixelReader mapPixelReader;
    private PixelWriter mapPixelWriter;
    
    // AND OTHER GAME DATA
    private String regionName;
    private String subRegionsType;
    private HashMap<Color, String> colorToSubRegionMappings;
    private HashMap<String, Color> subRegionToColorMappings;
    private HashMap<String, ArrayList<int[]>> pixels;
    private LinkedList<String> redSubRegions;
    private LinkedList<MovableText> subRegionStack;
    
    private long gameStartTime = -1;
    private int totalRegions = 0;
    private int regionsFound = 0;
    private int regionsLeft = 0;
    private int incorrectGuesses = 0;

    /**
     * Default constructor, it initializes all data structures for managing the
     * Sprites, including the map.
     */
    public RegioVincoDataModel() {
	// INITIALIZE OUR DATA STRUCTURES
	colorToSubRegionMappings = new HashMap();
	subRegionToColorMappings = new HashMap();
	subRegionStack = new LinkedList();
	redSubRegions = new LinkedList();
    }
    
    public void setMapImage(WritableImage initMapImage) {
	mapImage = initMapImage;
	mapPixelReader = mapImage.getPixelReader();
	mapPixelWriter = mapImage.getPixelWriter();
    }

    public void removeAllButOneFromStack(RegioVincoGame game) {
        // LET'S CHANGE THE RED ONES BACK TO THEIR PROPER COLORS
        for (String s : redSubRegions) {
            Color subRegionColor = subRegionToColorMappings.get(s);
            changeSubRegionColorOnMap(game, s, subRegionColor);
        }
        redSubRegions.clear();
        
	while (subRegionStack.size() > 1) {
	    MovableText text = subRegionStack.removeFirst();
	    String subRegionName = text.getText().getText();
	    game.gameLayer.getChildren().remove(text.getText());
	    game.gameLayer.getChildren().remove(text.getRectangle());
            game.gameLayer.getChildren().remove(text.getImageView());
	    this.redSubRegions.clear();
	    
	    // UPDATE GAME STATS
	    regionsLeft = 1;
	    regionsFound = totalRegions - 1;

	    // TURN THE TERRITORY GREEN
	    changeSubRegionColorOnMap(game, subRegionName, BOTTOM_SUB_REGION_FILL_COLOR);
	}
	startTextStackMovingDown();
    }

    // ACCESSOR METHODS
    public String getRegionName() {
	return regionName;
    }

    public String getSubRegionsType() {
	return subRegionsType;
    }

    public void setRegionName(String initRegionName) {
	regionName = initRegionName;
    }

    public void setSubRegionsType(String initSubRegionsType) {
	subRegionsType = initSubRegionsType;
    }

    public String getSecondsAsTimeText(long numSeconds) {
	long numHours = numSeconds / 3600;
	numSeconds = numSeconds - (numHours * 3600);
	long numMinutes = numSeconds / 60;
	numSeconds = numSeconds - (numMinutes * 60);

	String timeText = "";
	if (numHours > 0) {
	    timeText += numHours + ":";
	}
	timeText += numMinutes + ":";
	if (numSeconds < 10) {
	    timeText += "0" + numSeconds;
	} else {
	    timeText += numSeconds;
	}
	return timeText;
    }

    public int getRegionsFound() {
	return colorToSubRegionMappings.keySet().size() - subRegionStack.size();
    }

    public int getRegionsNotFound() {
	return subRegionStack.size();
    }
    
    public LinkedList<MovableText> getSubRegionStack() {
	return subRegionStack;
    }
    
    public String getSubRegionMappedToColor(Color colorKey) {
	return colorToSubRegionMappings.get(colorKey);
    }
    
    public Color getColorMappedToSubRegion(String subRegion) {
	return subRegionToColorMappings.get(subRegion);
    }

    // MUTATOR METHODS

    public void addColorToSubRegionMappings(Color colorKey, String subRegionName) {
	colorToSubRegionMappings.put(colorKey, subRegionName);
    }

    public void addSubRegionToColorMappings(String subRegionName, Color colorKey) {
	subRegionToColorMappings.put(subRegionName, colorKey);
    }

    public void respondToNavigation(RegioVincoGame game, int x, int y) {
        Color pixelColor = mapPixelReader.getColor(x, y);
	String clickedSubRegion = colorToSubRegionMappings.get(pixelColor);
        
        if( (pixelColor == NONPLAYABLE_REGION_COLOR) || (clickedSubRegion == null) ) {
            return;
        }
        
        String clickedSubRegionPath = regionDataManager.getCanonicalPathFromRoot(currentRegion) +
                SLASH + clickedSubRegion;
        currentRegion = regionDataManager.getRegion(clickedSubRegionPath);
        //game.reloadMap();
        //reset(game);
        mapNavigate(game);
    }
    
    public void respondToUpperNavigation(RegioVincoGame game) {
        if(currentRegion.getName().equals("The World")) {
            return;
        }
        currentRegion = currentRegion.getParentRegion();
        //game.reloadMap();
        //reset(game);
        mapNavigate(game);
    }
    
    public void respondToMapSelection(RegioVincoGame game, int x, int y) {
        // THIS IS WHERE WE'LL CHECK TO SEE IF THE
	// PLAYER CLICKED NO THE CORRECT SUBREGION
	Color pixelColor = mapPixelReader.getColor(x, y);
	String clickedSubRegion = colorToSubRegionMappings.get(pixelColor);
	if ((clickedSubRegion == null) || (subRegionStack.isEmpty())) {
	    return;
	}
	if (clickedSubRegion.equals(subRegionStack.get(0).getText().getText())) {
	    // UPDATE STATS
	    regionsLeft--;
	    regionsFound++;
	    
	    // YAY, CORRECT ANSWER
            // PLAY SOUND ONLY ALLOWED
            if(!game.getSoundMute()){
                game.getAudio().play(SUCCESS, false);
            }

	    // TURN THE TERRITORY GREEN
	    changeSubRegionColorOnMap(game, clickedSubRegion, BOTTOM_SUB_REGION_FILL_COLOR);

	    // REMOVE THE MOVEABLE TEXT FROM THE GAME LAYER
	    MovableText mT = subRegionStack.getFirst();
	    game.gameLayer.getChildren().remove(mT.getText());
	    game.gameLayer.getChildren().remove(mT.getRectangle());
            game.gameLayer.getChildren().remove(mT.getImageView());

	    // AND REMOVE THE BOTTOM ELEMENT FROM THE STACK
	    subRegionStack.removeFirst();
	    
	    // AND LET'S CHANGE THE RED ONES BACK TO THEIR PROPER COLORS
	    for (String s : redSubRegions) {
		Color subRegionColor = subRegionToColorMappings.get(s);
		changeSubRegionColorOnMap(game, s, subRegionColor);
	    }
	    redSubRegions.clear();

	    startTextStackMovingDown();

	    if (subRegionStack.isEmpty()) {
		updateAllStats(game);
                GregorianCalendar now = new GregorianCalendar();
                long nowLong = now.getTimeInMillis();
                long diff = nowLong - gameStartTime;
                long seconds = diff/1000;

                RegionPlayable playable = regionDataManager.getPlayable(regionDataManager.getCanonicalPathFromRoot(currentRegion));
                if(calculateScore(seconds) > playable.getHighScore()) {
                    playable.setHighScore(calculateScore(seconds));
                }
                
                if(playable.getFewestGuesses() == 0) {
                    playable.setFewestGuesses(incorrectGuesses);
                }
                else {
                    if(incorrectGuesses < playable.getFewestGuesses()) {
                        playable.setFewestGuesses(incorrectGuesses);
                    }
                }
                
                String[] time = getSecondsAsTimeText(diff/1000).split(":");
                System.out.println("" + Integer.parseInt(time[0]) + " : " + Integer.parseInt(time[1]));
                if(playable.getFastestMin() == 0 && playable.getFastestSec() == 0) {
                    playable.setFastestMin(Integer.parseInt(time[0]));
                    playable.setFastestSec(Integer.parseInt(time[1]));
                }
                else {
                    if(Integer.parseInt(time[0]) < playable.getFastestMin()) {
                        playable.setFastestMin(Integer.parseInt(time[0]));
                        playable.setFastestSec(Integer.parseInt(time[1]));
                    }
                    else if(Integer.parseInt(time[0]) == playable.getFastestMin()) {
                        if(Integer.parseInt(time[1]) < playable.getFastestSec()) {
                            playable.setFastestSec(Integer.parseInt(time[1]));
                        }
                    }
                }
                
                System.out.println(playable.getFastestMin() + ":" + playable.getFastestSec());
		this.endGameAsWin();
                game.setStopGameMode(false);
		game.setDialogStatsVisible(true);
		game.setMapVisible(false);
                game.labelChanges(false, false, false, false, false);
                
                // ONLY PLAY MUSIC WHEN ALLOWED
                if(!game.getMusicMute()) {
                    game.getAudio().stop(TRACKED_SONG);
                    String anthemPath = regionDataManager.getCanonicalPathFromRoot(currentRegion) + 
                            SLASH + currentRegion.getName() + NATIONAL_ANTHEM_EXT;
                    File anthemFile = new File(anthemPath);
                    
                    try {
                        if(anthemFile.exists()) {
                            System.out.println("" + anthemPath + " is now playing");
                            game.getAudio().loadAudio(regionDataManager.getCanonicalPathFromRoot(currentRegion), anthemPath);
                            game.getAudio().play(regionDataManager.getCanonicalPathFromRoot(currentRegion), false);
                        }
                        else {
                            game.getAudio().loadAudio(regionDataManager.getCanonicalPathFromRoot(currentRegion), DEFAULT_ANTHEM_FILE_NAME);
                            game.getAudio().play(regionDataManager.getCanonicalPathFromRoot(currentRegion), false);
                        }
                    }
                    catch(Exception e) {
                        System.out.println("Error in loading national anthem.");
                    }
                }
	    }
	} else {
	    if (!redSubRegions.contains(clickedSubRegion)) {
		// UPDATE STATS
		incorrectGuesses++;
		
		// BOO WRONG ANSWER
                if(!game.getSoundMute()) {
                    game.getAudio().play(FAILURE, false);
                }

		// TURN THE TERRITORY TEMPORARILY RED
		changeSubRegionColorOnMap(game, clickedSubRegion, WRONG_SUB_REGION_FILL_COLOR);
		redSubRegions.add(clickedSubRegion);
	    }
	}
    }

    public void respondToMapMove(RegioVincoGame game, int x, int y) {
        Color pixelColor = mapPixelReader.getColor(x, y);
	String mouseOverSubRegion = colorToSubRegionMappings.get(pixelColor);
        String text = "";
        
        if(mouseOverSubRegion != null) { 
            String subRegionPath = regionDataManager.getCanonicalPathFromRoot(currentRegion) + 
                    SLASH + mouseOverSubRegion;
            Region region = regionDataManager.getRegion(subRegionPath);
            text += region.getName() + "\n\n";
            text += "Districts: " + region.sizeSubRegions() + "\n" +
                    "High Score: " + regionDataManager.getPlayable(subRegionPath).getHighScore() + "\n" +
                    "Fastest Time: " + regionDataManager.getPlayable(subRegionPath).getFastestMin() + ":" + 
                        regionDataManager.getPlayable(subRegionPath).getFastestSec() + "\n" +
                    "Fewest Guesses: " + regionDataManager.getPlayable(subRegionPath).getFewestGuesses();
            
            String flagPath = regionDataManager.getCanonicalPathFromRoot(region) + 
                    SLASH + region.getName() + FLAG_EXT;
            File file = new File(flagPath);
            ImageView flagView = game.getFlagImageView();
            if(file.exists()) {
                // SUB REGION HAS A OWN FLAG
                flagView.setImage(game.loadImage(flagPath));
                game.setFlagVisible(true);
            }
            else {
                game.setFlagVisible(false);
            }
        }
        else {
            game.setFlagVisible(false);
        }
        
        game.mouseOverRegionLabel.setText(text);
    }
    
    public void startTextStackMovingDown() {
	// AND START THE REST MOVING DOWN
	for (MovableText mT : subRegionStack) {
	    mT.setVelocityY(SUB_STACK_VELOCITY);
	}
    }

    public void changeNonPlayableRegionColor(String subRegion, Color color) {
        // THIS IS WHERE WE'LL CHECK TO SEE IF THE
	// PLAYER CLICKED NO THE CORRECT SUBREGION
	ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
	for (int[] pixel : subRegionPixels) {
	    mapPixelWriter.setColor(pixel[0], pixel[1], color);
	}
    }
    
    public void changeSubRegionColorOnMap(RegioVincoGame game, String subRegion, Color color) {
        // THIS IS WHERE WE'LL CHECK TO SEE IF THE
	// PLAYER CLICKED NO THE CORRECT SUBREGION
	ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
	for (int[] pixel : subRegionPixels) {
	    mapPixelWriter.setColor(pixel[0], pixel[1], color);
	}
    }

    public int getNumberOfSubRegions() {
	return colorToSubRegionMappings.keySet().size();
    }

    public void mapNavigate(RegioVincoGame game) {
        GAME_MODE = 0;
        game.setDialogStatsVisible(false);
        
	// LET'S CLEAR THE DATA STRUCTURES
	colorToSubRegionMappings.clear();
	subRegionToColorMappings.clear();
	subRegionStack.clear();
	redSubRegions.clear();
        
        // INIT THE MAPPINGS - NOTE THIS SHOULD 
	// BE DONE IN A FILE, WHICH WE'LL DO IN
	// FUTURE HOMEWORK ASSIGNMENTS
        Iterator regionsIterator = currentRegion.getSubRegions();
        System.out.println("sub size: " + currentRegion.sizeSubRegions());
        while(regionsIterator.hasNext()) {
            Region region = (Region)regionsIterator.next();
            short red = region.getRed();
            short green = region.getGreen();
            short blue = region.getBlue();
            String name = region.getName();
            colorToSubRegionMappings.put(makeColor(red, green, blue), name);
        }
        
        for (Color c : colorToSubRegionMappings.keySet()) {
            String subRegion = colorToSubRegionMappings.get(c);
            subRegionToColorMappings.put(subRegion, c);
        }
        
        ((RegioVincoGame) game).reloadMap();
        
        pixels = new HashMap();
        for (String subRegion : subRegionToColorMappings.keySet()) {
            pixels.put(subRegion, new ArrayList());
        }

	for (int i = 0; i < mapImage.getWidth(); i++) {
	    for (int j = 0; j < mapImage.getHeight(); j++) {
		Color c = mapPixelReader.getColor(i, j);
		if (colorToSubRegionMappings.containsKey(c)) {
		    String subRegion = colorToSubRegionMappings.get(c);
		    ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
		    int[] pixel = new int[2];
		    pixel[0] = i;
		    pixel[1] = j;
		    subRegionPixels.add(pixel);
		}
	    }
	}
        
        // PAINT NON-PLAYABLE REGIONS TO PINK COLOR
        regionsIterator = currentRegion.getSubRegions();
        while(regionsIterator.hasNext()) {
            Region region = (Region)regionsIterator.next();
            //File file = new File(CURRENT_PATH + region.getName());
            File file = new File(regionDataManager.getCanonicalPathFromRoot(currentRegion) + SLASH +
                    region.getName() + SLASH + region.getName() + XML_EXT);
            if(!file.exists()) {
                changeNonPlayableRegionColor(region.getName(), NONPLAYABLE_REGION_COLOR);
            }
        }
        
    }
    
    /**
     * Resets all the game data so that a brand new game may be played.
     *
     * @param game the Zombiquarium game in progress
     */
    @Override
    public void reset(PointAndClickGame game) {	
        //
        if(game.getDataModel().won()) {
            System.out.println("re direct to mapNavigate");
            mapNavigate((RegioVincoGame)game);
        }
        
        // STATS
	gameStartTime = new GregorianCalendar().getTimeInMillis();
	totalRegions = currentRegion.sizeSubRegions();
	regionsFound = 0;
	regionsLeft = totalRegions;
	incorrectGuesses = 0;
	
	// LET'S CLEAR THE DATA STRUCTURES
	colorToSubRegionMappings.clear();
	subRegionToColorMappings.clear();
	subRegionStack.clear();
	redSubRegions.clear();
        
        // INIT THE MAPPINGS - NOTE THIS SHOULD 
	// BE DONE IN A FILE, WHICH WE'LL DO IN
	// FUTURE HOMEWORK ASSIGNMENTS
        Iterator regionsIterator = currentRegion.getSubRegions();
        while(regionsIterator.hasNext()) {
            Region region = (Region)regionsIterator.next();
            short red = region.getRed();
            short green = region.getGreen();
            short blue = region.getBlue();
            String name = "";
            if(GAME_MODE == 1)          name = region.getName();
            else if(GAME_MODE == 2)     name = region.getCapital();
            else if(GAME_MODE == 3)     name = region.getLeader();
            else if(GAME_MODE == 4)     name = regionDataManager.getCanonicalPathFromRoot(region) + SLASH + region.getName() + FLAG_EXT;
            colorToSubRegionMappings.put(makeColor(red, green, blue), name);
        }
        
	// RESET THE MOVABLE TEXT
	Pane gameLayer = ((RegioVincoGame)game).getGameLayer();
	gameLayer.getChildren().clear();
	for (Color c : colorToSubRegionMappings.keySet()) {
	    String subRegion = colorToSubRegionMappings.get(c);
	    subRegionToColorMappings.put(subRegion, c);
	    MovableText subRegionText = new MovableText(gameLayer);
	    subRegionText.getText().setText(subRegion);
	    subRegionText.getRectangle().setFill(subRegionToColorMappings.get(subRegion));
            if(GAME_MODE == 4) {
                subRegionText.setImageView(((RegioVincoGame)game).loadImage(subRegion));
            }
	    subRegionText.setX(STACK_X);
	    subRegionText.setY(0);
	    subRegionText.getText().setFill(SUB_REGION_NAME_COLOR);
	    subRegionText.getText().setFont(SUB_REGION_FONT);
	    subRegionStack.add(subRegionText);
	}
	Collections.shuffle(subRegionStack);

        // NOW FIX THEIR Y LOCATIONS
        if(GAME_MODE == 4) {
            int y = GAME_HEIGHT;
            for (MovableText mT : subRegionStack) {
                y -= mT.getImageInView().getHeight();
                mT.setY(y);
            }
        }
        else {
            int y = STACK_INIT_Y;
            int yInc = STACK_INIT_Y_INC;
            for (MovableText mT : subRegionStack) {
                int tY = y + yInc;
                mT.setY(tY);
                yInc -= 50;
            }
        }

	// RELOAD THE MAP
	((RegioVincoGame) game).reloadMap();

	// LET'S RECORD ALL THE PIXELS
	pixels = new HashMap();
	for (MovableText mT : subRegionStack) {
	    pixels.put(mT.getText().getText(), new ArrayList());
	}

	for (int i = 0; i < mapImage.getWidth(); i++) {
	    for (int j = 0; j < mapImage.getHeight(); j++) {
		Color c = mapPixelReader.getColor(i, j);
		if (colorToSubRegionMappings.containsKey(c)) {
		    String subRegion = colorToSubRegionMappings.get(c);
		    ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
		    int[] pixel = new int[2];
		    pixel[0] = i;
		    pixel[1] = j;
		    subRegionPixels.add(pixel);
		}
	    }
	}
        
	// RESET THE AUDIO WHEN ALLOWED
        
        if(!((RegioVincoGame)game).getMusicMute()) {
            AudioManager audio = ((RegioVincoGame) game).getAudio();
            //audio.stop(NATIONAL_ANTHEM);
            if (!audio.isPlaying(TRACKED_SONG)) {
                audio.play(TRACKED_SONG, true);
            }
        }
	
	// LET'S GO
	beginGame();
        ((RegioVincoGame)game).setGameModeScreen();
    }
   
    // HELPER METHOD FOR MAKING A COLOR OBJECT
    public static Color makeColor(int r, int g, int b) {
	return Color.color(r/255.0, g/255.0, b/255.0);
    }

    // STATE TESTING METHODS
    // UPDATE METHODS
	// updateAll
	// updateDebugText
    
    /**
     * Called each frame, this thread already has a lock on the data. This
     * method updates all the game sprites as needed.
     *
     * @param game the game in progress
     */
    @Override
    public void updateAll(PointAndClickGame game, double percentage) {
	for (MovableText mT : subRegionStack) {
	    mT.update(percentage);
	}
	if (!subRegionStack.isEmpty()) {
	    MovableText bottomOfStack = subRegionStack.get(0);
	    bottomOfStack.getRectangle().setFill(BOTTOM_SUB_REGION_FILL_COLOR);
	    bottomOfStack.getText().setFill(BOTTOM_SUB_REGION_TEXT_COLOR);
	    
	    if(GAME_MODE == 4) {
                double bottomY = bottomOfStack.getImageView().getY();
                double heightY = bottomOfStack.getImageInView().getHeight();
                if (bottomY >= GAME_HEIGHT-heightY) {
                    double diffY = bottomY - GAME_HEIGHT + heightY;
                    for (MovableText mT : subRegionStack) {
                        mT.setY(mT.getImageView().getY() - diffY);
                        mT.setVelocityY(0);
                    }
                }
            }
            else {
                double bottomY = bottomOfStack.getText().getY();
                if (bottomY >= FIRST_REGION_Y_IN_STACK) {
                    double diffY = bottomY - FIRST_REGION_Y_IN_STACK;
                    for (MovableText mT : subRegionStack) {
                        mT.setY(mT.getRectangle().getY() - diffY);
                        mT.setVelocityY(0);
                    }
                }
            }
	    // UPDATE THE STATS
	    updateAllStats((RegioVincoGame)game);
	}
    }
    
    public void updateAllStats(RegioVincoGame game) {
	    GregorianCalendar now = new GregorianCalendar();
	    long nowLong = now.getTimeInMillis();
	    long diff = nowLong - gameStartTime;
	    long seconds = diff/1000;
	    String statsText = "" + getSecondsAsTimeText(seconds) 
		    + "      Regions Found: " + regionsFound
		    + "      Regions Left: " + regionsLeft
		    + "      Incorrect Guesses: " + incorrectGuesses;
	    game.updateStatsLabel(statsText);
	    
	    String dialogStatsText = "Region: " + currentRegion.getName() + "\n" 
		    +	"Score: " + calculateScore(seconds) + "\n"
		    +	"Game Duration: " + getSecondsAsTimeText(diff/1000) + "\n"
		    +	"Sub Regions: " + regionsFound + "\n"
		    +	"Incorrect Guesses: " + incorrectGuesses;
	    game.updateDialogStatsLabel(dialogStatsText);
            
            String imgPath = regionDataManager.getCanonicalPathFromRoot(currentRegion) + 
                    SLASH + currentRegion.getName() + FLAG_EXT;
            File file = new File(imgPath);
            ImageView winFlagView = game.getWinFlagImageView();
            
            if(file.exists())   winFlagView.setImage(game.loadImage(imgPath));
            else                winFlagView.setImage(game.loadImage(FLAG_NOT_FOUND_FILE_PATH));
    }
	
	public int calculateScore(long time) {
	    int score = 1000;
	    score -= (10 * incorrectGuesses);
	    score -= (int)time;
	    return score;
	}
	
    /**
     * Called each frame, this method specifies what debug text to render. Note
     * that this can help with debugging because rather than use a
     * System.out.print statement that is scrolling at a fast frame rate, we can
     * observe variables on screen with the rest of the game as it's being
     * rendered.
     *
     * @return game the active game being played
     */
    public void updateDebugText(PointAndClickGame game) {
	debugText.clear();
    }
}
