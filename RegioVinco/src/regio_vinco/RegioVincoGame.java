package regio_vinco;

import audio_manager.AudioManager;
import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import pacg.PointAndClickGame;
import static regio_vinco.RegioVinco.*;
import regio_vinco_data.ConfirmDialog;
import static regio_vinco_data.ConfirmDialog.OK;
import regio_vinco_data.Region;
import regio_vinco_data.RegionPlayable;

/**
 * This class is a concrete PointAndClickGame, as specified in The PACG
 * Framework. Note that this one plays Regio Vinco.
 *
 * @author McKillaGorilla
 */
public class RegioVincoGame extends PointAndClickGame {
    
    // THIS PROVIDES GAME AND GUI EVENT RESPONSES
    RegioVincoController controller;

    // THIS PROVIDES MUSIC AND SOUND EFFECTS
    AudioManager audio;
    
    // THESE ARE THE GUI LAYERS
    Pane splashLayer;
    Pane backgroundLayer;
    Pane gameLayer;
    Pane settingLayer;
    Pane helpLayer;
    Pane guiLayer;
    
    // WE'LL SET THESE WHEN WE ENTER THE GAME
    Label regionLabel;
    Label regionNameLabel;
    Label mouseOverRegionLabel;
    Label thisRegionScoreLabel;
    Label navigationLabel;
    Label statsLabel;
    Label dialogStatsLabel;
    Label summaryLabel;
    
    // CHECK BOXES FOR UM/MUTE SOUND
    CheckBox soundCheckBox;
    CheckBox musicCheckBox;

    boolean inGameMode;
    public static Region currentRegion;
    
    // WE'LL KEEP GAME SPLASHED OR YET.
    public static boolean APP_STARTED = false;
    public static int GAME_MODE = 0;
    
    /**
     * Get the game setup.
     */
    public RegioVincoGame(Stage initWindow) {
	super(initWindow, APP_TITLE, TARGET_FRAME_RATE);
        initRegionsData();
        initAudio();
        setToStart();
    }
    
    private void initRegionsData() {
        loadRegions("", "The World");
        initGameModes(regionDataManager.getWorld());
    }
    
    private void loadRegions(String loadingRegionDir, String loadingRegion) {
        String loadingRegionPath;
        if(loadingRegionDir.equals("")) {
            loadingRegionPath = loadingRegion;
        }
        else {
            loadingRegionPath = loadingRegionDir + SLASH + loadingRegion;
        }
        File loadingFile = new File(loadingRegionPath + SLASH + loadingRegion + XML_EXT);

        // XML FILE DOESN'T EXIST or INVALID XML FILE
        if(!regionDataManager.load(loadingFile)) {
            return;
        }
        // REGION XML FILE IS LOADED
        else {
            Iterator<Region> subRegionIterator = regionDataManager.getRegion(loadingRegionPath).getSubRegions();
            
            // LOAD SUB REGIONS
            while(subRegionIterator.hasNext()) {
                String subRegionName = subRegionIterator.next().getName();
                loadRegions(loadingRegionPath, subRegionName);
            }
        }
    }
    
    private void initGameModes(Region region) {
        
        // NAME GAME & CAPITAL GAME & LEADER GAME MODE
        boolean nameGameMode = checkNameGameMode(region);
        boolean capitalGameMode = checkCapitalGameMode(region);
        boolean leaderGameMode = checkLeaderGameMode(region);
        boolean flagGameMode = checkFlagGameMode(region);
        
        RegionPlayable regionPlayable = new RegionPlayable(nameGameMode, capitalGameMode, leaderGameMode, flagGameMode);
        String regionId = regionDataManager.getCanonicalPathFromRoot(region);
        //System.out.println(regionId);
        //System.out.println(regionPlayable);
        regionDataManager.addPlayable(regionId, regionPlayable);
        
        // RECALL THIS METHOD OF SUB REGION
        Iterator<Region> subRegionIterator = region.getSubRegions();
        while(subRegionIterator.hasNext()) {
            initGameModes(subRegionIterator.next());
        }
    }
    
    private boolean checkNameGameMode(Region region) {
        Iterator<Region> subRegionIterator = region.getSubRegions();
        while(subRegionIterator.hasNext()) {
            if(subRegionIterator.next().getName() == null) {
                return false;
            }
        }
        return true;
    }
    
    private boolean checkCapitalGameMode(Region region) {
        Iterator<Region> subRegionIterator = region.getSubRegions();
        while(subRegionIterator.hasNext()) {
            if(!subRegionIterator.next().hasCapital()) {
                return false;
            }
        }
        return true;
    }
    
    private boolean checkLeaderGameMode(Region region) {
        Iterator<Region> subRegionIterator = region.getSubRegions();
        while(subRegionIterator.hasNext()) {
            if(!subRegionIterator.next().hasLeader()) {
                return false;
            }
        }
        return true;
    }
    
    private boolean checkFlagGameMode(Region region) {
        String regionPath = regionDataManager.getCanonicalPathFromRoot(region);
        
        Iterator<Region> subRegionIterator = region.getSubRegions();
        while(subRegionIterator.hasNext()) {
            Region subRegion = subRegionIterator.next();
            File file = new File(regionPath + SLASH + subRegion.getName() + SLASH + subRegion.getName() + MAP_EXT);
            if(!file.exists()) {
                return false;
            }
        }
        return true;
    }
    
    public AudioManager getAudio() {
	return audio;
    }
    
    public Pane getGameLayer() {
	return gameLayer;
    }
    
    private void setToStart() {
        inGameMode = false;
        currentRegion = regionDataManager.getWorld();
        stackPane.getChildren().get(0).toFront();
    }
    
    public boolean getInGameMode() {
        return inGameMode;
    }
    
    public void setInGameMode(boolean initInGameMode) {
        inGameMode = initInGameMode;
    }
    /**
     * Initializes audio for the game.
     */
    private void initAudio() {
	audio = new AudioManager();
	try {
	    audio.loadAudio(TRACKED_SONG, TRACKED_FILE_NAME);
	    audio.play(TRACKED_SONG, true);

	    audio.loadAudio(SUCCESS, SUCCESS_FILE_NAME);
	    audio.loadAudio(FAILURE, FAILURE_FILE_NAME);
	} catch (Exception e) {
	    
	}
    }

    // OVERRIDDEN METHODS - REGIO VINCO IMPLEMENTATIONS
    // initData
    // initGUIControls
    // initGUIHandlers
    // reset
    // updateGUI
    /**
     * Initializes the complete data model for this application, forcing the
     * setting of all game data, including all needed SpriteType objects.
     */
    @Override
    public void initData() {
	// INIT OUR DATA MANAGER
	data = new RegioVincoDataModel();
	data.setGameDimensions(GAME_WIDTH, GAME_HEIGHT);
        
	boundaryLeft = 0;
	boundaryRight = GAME_WIDTH;
	boundaryTop = 0;
	boundaryBottom = GAME_HEIGHT;
    }

    /**
     * For initializing all GUI controls, specifically all the buttons and
     * decor. Note that this method must construct the canvas with its custom
     * renderer.
     */
    @Override
    public void initGUIControls() {
	// LOAD THE GUI IMAGES, WHICH INCLUDES THE BUTTONS
	// THESE WILL BE ON SCREEN AT ALL TIMES
        splashLayer = new Pane();
        addStackPaneLayer(splashLayer);
        addGUIImage(splashLayer, SPLASH_TYPE, loadImage(SPLASH_FILE_PATH), SPLASH_X, SPLASH_Y);
        addGUIButton(splashLayer, ENTER_TYPE, loadImage(ENTER_BUTTON_FILE_PATH), ENTER_X, ENTER_Y);
        
        Button enterButton = this.guiButtons.get(ENTER_TYPE);
        enterButton.setBorder(Border.EMPTY);
        enterButton.setPadding(Insets.EMPTY);
        enterButton.setEffect(null);
        
	backgroundLayer = new Pane();
	addStackPaneLayer(backgroundLayer);
	addGUIImage(backgroundLayer, BACKGROUND_TYPE, loadImage(BACKGROUND_FILE_PATH), BACKGROUND_X, BACKGROUND_Y);
	
	// THEN THE GAME LAYER
	gameLayer = new Pane();
        addStackPaneLayer(gameLayer);
        gameLayer.setVisible(false);
        
        // THEN THE SETTING LAYER
        settingLayer = new Pane();
        addStackPaneLayer(settingLayer);
        addGUIImage(settingLayer, SETTING_PANE_TYPE, loadImage(SETTING_FILE_PATH), SETTING_PANE_X, SETTING_PANE_Y);
        settingLayer.setVisible(false);
        
        // THEN THE HELP LAYER
        helpLayer = new Pane();
        addStackPaneLayer(helpLayer);
        addGUIImage(helpLayer, HELP_PANE_TYPE, loadImage(HELP_FILE_PATH), HELP_PANE_X, HELP_PANE_Y);
        setHelpLayerVisible(false);
        
        // SUMMARY OF THE RIGIO VINCO GAME
        summaryLabel = new Label();
	helpLayer.getChildren().add(summaryLabel);
        summaryLabel.translateXProperty().setValue(SUMMARY_X);
        summaryLabel.translateYProperty().setValue(SUMMARY_Y);
        summaryLabel.setTextFill(SUMMARY_COLOR);
        summaryLabel.setFont(SUMMARY_FONT);
        summaryLabel.setMinWidth(SUMMARY_WIDTH);
        summaryLabel.setMaxWidth(SUMMARY_WIDTH);
        summaryLabel.setText(getSummaryText());
        summaryLabel.setVisible(true);
        
	// THEN THE GUI LAYER
	guiLayer = new Pane();
	addStackPaneLayer(guiLayer);
	addGUIImage(guiLayer, TITLE_TYPE, loadImage(TITLE_FILE_PATH), TITLE_X, TITLE_Y);
        addGUIButton(guiLayer, HOME_TYPE, loadImage(HOME_DISABLED_BUTTON_FILE_PATH), HOME_X, HOME_Y);
        addGUIButton(guiLayer, SETTING_TYPE, loadImage(SETTING_BUTTON_FILE_PATH), SETTING_X, SETTING_Y);
        addGUIButton(guiLayer, HELP_TYPE, loadImage(HELP_BUTTON_FILE_PATH), HELP_X, HELP_Y);
        addGUIButton(guiLayer, NAME_GAME_TYPE, loadImage(NAME_GAME_BUTTON_FILE_PATH), NAME_GAME_X, NAME_GAME_Y);
        addGUIButton(guiLayer, CAPITAL_GAME_TYPE, loadImage(CAPITAL_GAME_BUTTON_FILE_PATH), CAPITAL_GAME_X, CAPITAL_GAME_Y);
        addGUIButton(guiLayer, LEADER_GAME_TYPE, loadImage(LEADER_GAME_BUTTON_FILE_PATH), LEADER_GAME_X, LEADER_GAME_Y);
        addGUIButton(guiLayer, FLAG_GAME_TYPE, loadImage(FLAG_GAME_BUTTON_FILE_PATH), FLAG_GAME_X, FLAG_GAME_Y);
        addGUIButton(guiLayer, STOP_GAME_TYPE, loadImage(STOP_GAME_BUTTON_FILE_PATH), STOP_GAME_X, STOP_GAME_Y);
        addGUIButton(guiLayer, CLOSE_WIN_DISPLAY_TYPE, loadImage(CLOSE_WIN_DISPLAY_BUTTON_FILE_PATH), CLOSE_WIN_DISPLAY_X, CLOSE_WIN_DISPLAY_Y);
	//addGUIButton(guiLayer, START_TYPE, loadImage(START_BUTTON_FILE_PATH), START_X, START_Y);
	//addGUIButton(guiLayer, EXIT_TYPE, loadImage(EXIT_BUTTON_FILE_PATH), EXIT_X, EXIT_Y);
	
        // CHECK BOXES FOR UN/MUTE THE SOUND
        soundCheckBox = new CheckBox("Mute Sound");
        guiLayer.getChildren().add(soundCheckBox);
        soundCheckBox.setSelected(false);
        soundCheckBox.setFont(CHECKBOX_FONT);
        soundCheckBox.translateXProperty().setValue(SOUND_X);
        soundCheckBox.translateYProperty().setValue(SOUND_Y);
        soundCheckBox.setVisible(false);
        
        // CHECK BOXES FOR UN/MUTE THE MUSIC
        musicCheckBox = new CheckBox("Mute Music");
        guiLayer.getChildren().add(musicCheckBox);
        musicCheckBox.setSelected(false);
        musicCheckBox.setFont(CHECKBOX_FONT);
        musicCheckBox.translateXProperty().setValue(MUSIC_X);
        musicCheckBox.translateYProperty().setValue(MUSIC_Y);
        musicCheckBox.setVisible(false);
        
        Button homeButton = this.guiButtons.get(HOME_TYPE);
        homeButton.setBorder(Border.EMPTY);
        homeButton.setPadding(Insets.EMPTY);
        homeButton.setEffect(null);
        Button settingButton = this.guiButtons.get(SETTING_TYPE);
        settingButton.setBorder(Border.EMPTY);
        settingButton.setPadding(Insets.EMPTY);
        settingButton.setEffect(null);
        Button helpButton = this.guiButtons.get(HELP_TYPE);
        helpButton.setBorder(Border.EMPTY);
        helpButton.setPadding(Insets.EMPTY);
        helpButton.setEffect(null);
        
        Button nameGameButton = this.guiButtons.get(NAME_GAME_TYPE);
        nameGameButton.setBorder(Border.EMPTY);
        nameGameButton.setPadding(Insets.EMPTY);
        nameGameButton.setEffect(null);
        Button capitalGameButton = this.guiButtons.get(CAPITAL_GAME_TYPE);
        capitalGameButton.setBorder(Border.EMPTY);
        capitalGameButton.setPadding(Insets.EMPTY);
        capitalGameButton.setEffect(null);
        Button leaderGameButton = this.guiButtons.get(LEADER_GAME_TYPE);
        leaderGameButton.setBorder(Border.EMPTY);
        leaderGameButton.setPadding(Insets.EMPTY);
        leaderGameButton.setEffect(null);
        Button flagGameButton = this.guiButtons.get(FLAG_GAME_TYPE);
        flagGameButton.setBorder(Border.EMPTY);
        flagGameButton.setPadding(Insets.EMPTY);
        flagGameButton.setEffect(null);
        Button stopGameButton = this.guiButtons.get(STOP_GAME_TYPE);
        stopGameButton.setBorder(Border.EMPTY);
        stopGameButton.setPadding(Insets.EMPTY);
        stopGameButton.setEffect(null);
        
        Button closeWinDisplayButton = this.guiButtons.get(CLOSE_WIN_DISPLAY_TYPE);
        closeWinDisplayButton.setBorder(Border.EMPTY);
        closeWinDisplayButton.setPadding(Insets.EMPTY);
        closeWinDisplayButton.setEffect(null);
        closeWinDisplayButton.setVisible(false);
       
	// NOW LET'S ADD THE REGION LABEL
	regionLabel = new Label();
        guiLayer.getChildren().add(regionLabel);
	regionLabel.translateXProperty().setValue(REGION_NAME_ON_GAME_X);
	regionLabel.translateYProperty().setValue(REGION_NAME_ON_GAME_Y);
	regionLabel.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, null)));
	regionLabel.setTextFill(REGION_NAME_ON_GAME_COLOR);
	regionLabel.setFont(REGION_NAME_ON_GAME_FONT);
	regionLabel.setMinWidth(REGION_NAME_ON_GAME_WIDTH);
	regionLabel.setMaxWidth(REGION_NAME_ON_GAME_WIDTH);
	regionLabel.setMinHeight(REGION_NAME_ON_GAME_HEIGHT);
	regionLabel.setMaxHeight(REGION_NAME_ON_GAME_HEIGHT);
        regionLabel.setVisible(false);
	
	// NOTE THAT THE MAP IS ALSO AN IMAGE, BUT
	// WE'LL LOAD THAT WHEN A GAME STARTS, SINCE
	// WE'LL BE CHANGING THE PIXELS EACH TIME
	// FOR NOW WE'LL JUST LOAD THE ImageView
	// THAT WILL STORE THAT IMAGE
	ImageView mapView = new ImageView();
	mapView.setX(MAP_X);
	mapView.setY(MAP_Y);
	guiImages.put(MAP_TYPE, mapView);
	guiLayer.getChildren().add(mapView);
	
        // NAVIGATION ROUTES
        navigationLabel = new Label("");
        guiLayer.getChildren().add(navigationLabel);
        navigationLabel.translateXProperty().setValue(NAVI_X);
        navigationLabel.translateYProperty().setValue(NAVI_Y);
        navigationLabel.setTextFill(NAVI_COLOR);
        navigationLabel.setFont(NAVI_FONT);
        navigationLabel.setVisible(true);
        
        // SCORE FOR THIS REGION
        thisRegionScoreLabel = new Label("");
        guiLayer.getChildren().add(thisRegionScoreLabel);
        thisRegionScoreLabel.translateXProperty().setValue(REGION_SCORE_X);
        thisRegionScoreLabel.translateYProperty().setValue(REGION_SCORE_Y);
        thisRegionScoreLabel.setTextFill(REGION_SCORE_COLOR);
        thisRegionScoreLabel.setFont(REGION_SCORE_FONT);
        thisRegionScoreLabel.setVisible(true);
        
        // REGION NAME
        regionNameLabel = new Label("");
        guiLayer.getChildren().add(regionNameLabel);
        regionNameLabel.setText(REGION_NAME);
        regionNameLabel.translateXProperty().setValue(REGION_NAME_X);
        regionNameLabel.translateYProperty().setValue(REGION_NAME_Y);
        regionNameLabel.setTextFill(REGION_NAME_COLOR);
        regionNameLabel.setFont(REGION_NAME_FONT);
        regionNameLabel.setVisible(true);
        
        // MOUSE OVER SUB REGION NAME & STATS
        mouseOverRegionLabel = new Label("");
        guiLayer.getChildren().add(mouseOverRegionLabel);
        mouseOverRegionLabel.translateXProperty().setValue(POINT_SUBREGION_X);
        mouseOverRegionLabel.translateYProperty().setValue(POINT_SUBREGION_Y);
        mouseOverRegionLabel.setTextFill(POINT_SUBREGION_COLOR);
        mouseOverRegionLabel.setFont(POINT_SUBREGION_FONT);
        mouseOverRegionLabel.setVisible(true);
        
        // MOUSE OVER SUB REGION FLAG
        ImageView flagView = new ImageView();
	flagView.setX(POINT_SUBREGION_FLAG_X);
	flagView.setY(POINT_SUBREGION_FLAG_Y);
	guiImages.put(POINT_SUBREGION_FLAG_TYPE, flagView);
	guiLayer.getChildren().add(flagView);
        
	// IN GAME STATS
	statsLabel = new Label("");
	guiLayer.getChildren().add(statsLabel);
	statsLabel.translateXProperty().setValue(STATS_X);
	statsLabel.translateYProperty().setValue(STATS_Y);
	statsLabel.setTextFill(STATS_COLOR);
	statsLabel.setFont(STATS_FONT);
        statsLabel.setVisible(false);

	// NOW LOAD THE WIN DISPLAY, WHICH WE'LL ONLY
	// MAKE VISIBLE AND ENABLED AS NEEDED
	ImageView winView = addGUIImage(guiLayer, WIN_DISPLAY_TYPE, loadImage(WIN_DISPLAY_FILE_PATH), WIN_X, WIN_Y);
	winView.setVisible(false);
        ImageView winFlagView = new ImageView();
        winFlagView.setX(WIN_DISPLAY_FLAG_X);
        winFlagView.setY(WIN_DISPLAY_FLAG_Y);
        guiImages.put(WIN_DISPLAY_FLAG_TYPE, winFlagView);
        guiLayer.getChildren().add(winFlagView);
	
	// AND THE STATS IN THE DIALOG
	dialogStatsLabel = new Label();
	guiLayer.getChildren().add(dialogStatsLabel);
	dialogStatsLabel.translateXProperty().setValue(DIALOG_STATS_X);
	dialogStatsLabel.translateYProperty().setValue(DIALOG_STATS_Y);
	dialogStatsLabel.setTextFill(DIALOG_STATS_COLOR);
	dialogStatsLabel.setFont(DIALOG_STATS_FONT);
	dialogStatsLabel.setWrapText(true);
	dialogStatsLabel.setVisible(false);
    }
    
    public String getSummaryText() {
        String summary = "";
        
        summary += "<What Is Regio Vinco?>\n" +
                   " The Regio Vinco game is a game for learning about the world and its\n" +
                   " geography. Through playing the game, the player can practice their\n" +
                   " knowledge of continent, nation, state, and province borders, as well as\n" +
                   " capitals, flags, and leaders.\n\n" +
                   "<How To Play Regio Vinco Game>\n" +
                   "1. Choose a region that you want to play.\n" +
                   "   You can navigate map by clicking a region or navi bar on the bottom.\n" +
                   "2. Choose a game mode by clicking one of the yellow button on the right.\n" +
                   "   (Name mode / Capital mode / Leader mode / Flag mode)\n" +
                   "3. During the game, click the region which is associated with the most\n" +
                   "   bottom one in the stack.\n" +
                   "4. You can stop the game and return to map selection through STOP icon.\n" +
                   "5. If you get high scores, they are recorded. Let's challenge!!!\n\n" +
                   "                                                                       by Chungjung Lee";
        
        return summary;
    }
    
    public void setMapVisible(boolean visible) {
	ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setVisible(visible);
    }
    
    public void setFlagVisible(boolean visible) {
        ImageView flagView = guiImages.get(POINT_SUBREGION_FLAG_TYPE);
        flagView.setVisible(visible);
    }
    
    public ImageView getFlagImageView() {
        return guiImages.get(POINT_SUBREGION_FLAG_TYPE);
    }
    
    public ImageView getWinFlagImageView() {
        return guiImages.get(WIN_DISPLAY_FLAG_TYPE);
    }
    
    // HELPER METHOD FOR LOADING IMAGES
    public Image loadImage(String imagePath) {	
	Image img = new Image("file:" + imagePath);
	return img;
    }
    
    public void updateStatsLabel(String statsText) {
	statsLabel.setText(statsText);
    }
    
    public void updateDialogStatsLabel(String statsText) {
	dialogStatsLabel.setText(statsText);
    }

    /**
     * For initializing all the button handlers for the GUI.
     */
    @Override
    public void initGUIHandlers() {
	controller = new RegioVincoController(this);

        Button enterButton = guiButtons.get(ENTER_TYPE);
        enterButton.setOnAction(e -> {
            stackPane.getChildren().remove(splashLayer);
            controller.processEnterRequest();
        });
        
        Button homeButton = guiButtons.get(HOME_TYPE);
        homeButton.setOnAction(e -> {
            if(!data.inProgress() && !guiImages.get(WIN_DISPLAY_TYPE).isVisible()) {
                controller.processHomeRequest();
            }
        });
        
        Button settingButton = guiButtons.get(SETTING_TYPE);
        settingButton.setOnAction(e -> {
            if(!data.inProgress() && !guiImages.get(WIN_DISPLAY_TYPE).isVisible()) {
                controller.processSettingRequest();
            }
        });
        
        Button helpButton = guiButtons.get(HELP_TYPE);
        helpButton.setOnAction(e -> {
            if(!data.inProgress() && !guiImages.get(WIN_DISPLAY_TYPE).isVisible()) {
                controller.processHelpRequest();
            }
        });
        
        Button nameGameButton = guiButtons.get(NAME_GAME_TYPE);
        nameGameButton.setOnAction(e -> {
            if(!regionDataManager.getPlayable(regionDataManager.getCanonicalPathFromRoot(currentRegion)).getNameMode()) {
                return;
            }
            if(!data.inProgress() && !guiImages.get(WIN_DISPLAY_TYPE).isVisible()) {
                GAME_MODE = 1;
                controller.processNameGameRequest();
            }
        });
        
        Button capitalGameButton = guiButtons.get(CAPITAL_GAME_TYPE);
        capitalGameButton.setOnAction(e -> {
            if(!regionDataManager.getPlayable(regionDataManager.getCanonicalPathFromRoot(currentRegion)).getCapitalMode()) {
                return;
            }
            if(!data.inProgress() && !guiImages.get(WIN_DISPLAY_TYPE).isVisible()) {
                GAME_MODE = 2;
                controller.processCapitalGameRequest();
            }
        });
        
        Button leaderGameButton = guiButtons.get(LEADER_GAME_TYPE);
        leaderGameButton.setOnAction(e -> {
            if(!regionDataManager.getPlayable(regionDataManager.getCanonicalPathFromRoot(currentRegion)).getLeaderMode()) {
                return;
            }
            if(!data.inProgress() && !guiImages.get(WIN_DISPLAY_TYPE).isVisible()) {
                GAME_MODE = 3;
                controller.processLeaderGameRequest();
            }
        });
        
        Button flagGameButton = guiButtons.get(FLAG_GAME_TYPE);
        flagGameButton.setOnAction(e -> {
            if(!regionDataManager.getPlayable(regionDataManager.getCanonicalPathFromRoot(currentRegion)).getFlagMode()) {
                return;
            }
            if(!data.inProgress() && !guiImages.get(WIN_DISPLAY_TYPE).isVisible()) {
                GAME_MODE = 4;
                controller.processFlagGameRequest();
            }
        });
        
        Button stopGameButton = guiButtons.get(STOP_GAME_TYPE);
        stopGameButton.setOnAction(e -> {
            if(data.inProgress()) {
                controller.processStopGameRequest();
            }
        });
        
        Button closeWinDisplayButton = guiButtons.get(CLOSE_WIN_DISPLAY_TYPE);
        closeWinDisplayButton.setOnAction(e -> {
            // RESET THE AUDIO WHEN ALLOWED
            if(!getMusicMute()) {
                AudioManager audio = getAudio();

                if (audio.isPlaying(regionDataManager.getCanonicalPathFromRoot(currentRegion))) {
                    audio.stop(regionDataManager.getCanonicalPathFromRoot(currentRegion));
                }

                if (!audio.isPlaying(TRACKED_SONG)) {
                    audio.play(TRACKED_SONG, true);
                }
            }
            
            navigationMode();
            data.endGameAsLoss();
            ((RegioVincoDataModel)data).mapNavigate(this);
        });
        
        // CHECK BOXES
        soundCheckBox.setOnAction(e -> {
            if(soundCheckBox.isSelected()) {
                audio.stop(SUCCESS);
            }
            else {
                audio.play(SUCCESS, false);
            }
        });
        
        musicCheckBox.setOnAction(e -> {
            if(musicCheckBox.isSelected())
                audio.stop(TRACKED_SONG);
            else
                audio.play(TRACKED_SONG, true);
        });
        
	// MAKE THE CONTROLLER THE HOOK FOR KEY PRESSES
	keyController.setHook(controller);

	// SETUP MOUSE PRESSES ON THE MAP
	ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setOnMousePressed(e -> {
	    controller.processMapClickRequest(data.inProgress(), (int) e.getX(), (int) e.getY());
	});
        mapView.setOnMouseMoved(e -> {
            if(!data.inProgress()) {
                controller.processMapMoveRequest((int) e.getX(), (int) e.getY());
            }
        });
        
        // SETUP MOUSE PRESSES ON THE NAVIGATION LABEL
        navigationLabel.setOnMousePressed(e -> {
            controller.processNaviRequest();
        });
	
	// KILL THE APP IF THE USER CLOSES THE WINDOW
	window.setOnCloseRequest(e->{
	    controller.processExitGameRequest();
	});
    }
    
    public void screenChangeButtons(boolean home, boolean setting, boolean help) {
        Button homeButton = guiButtons.get(HOME_TYPE);
        Button settingButton = guiButtons.get(SETTING_TYPE);
        Button helpButton = guiButtons.get(HELP_TYPE);
        
        if(home)    homeButton.setGraphic(new ImageView(loadImage(HOME_BUTTON_FILE_PATH)));
        else        homeButton.setGraphic(new ImageView(loadImage(HOME_DISABLED_BUTTON_FILE_PATH)));
        
        if(setting) settingButton.setGraphic(new ImageView(loadImage(SETTING_BUTTON_FILE_PATH)));
        else        settingButton.setGraphic(new ImageView(loadImage(SETTING_DISABLED_BUTTON_FILE_PATH)));
        
        if(help)    helpButton.setGraphic(new ImageView(loadImage(HELP_BUTTON_FILE_PATH)));
        else        helpButton.setGraphic(new ImageView(loadImage(HELP_DISABLED_BUTTON_FILE_PATH)));
    }
    
    public void screenChanges(boolean map, boolean game, boolean setting, boolean help) {
        setMapVisible(map);
        setGameLayerVisible(game);
        setSettingLayerVisible(setting);
        setHelpLayerVisible(help);
    }
    
    public void labelChanges(boolean regionName, boolean navigation, boolean regionScore, boolean status, boolean nameOnGame) {
        regionNameLabel.setVisible(regionName);
        navigationLabel.setVisible(navigation);
        thisRegionScoreLabel.setVisible(regionScore);
        statsLabel.setVisible(status);
        if(nameOnGame) {
            regionLabel.setText(currentRegion.getName());
        }
        regionLabel.setVisible(nameOnGame);
    }
    
    public void navigationMode() {
        setMapVisible(true);
        ((RegioVincoDataModel)this.getDataModel()).mapNavigate(this);
    }
    
    /**
     * Called when a game is restarted from the beginning, it resets all game
     * data and GUI controls so that the game may start anew.
     */
    @Override
    public void reset() {
	// IF THE WIN DIALOG IS VISIBLE, MAKE IT INVISIBLE
	// WINNIG DIALOG MAKE INVISIBLE
        //guiImages.get(WIN_DISPLAY_TYPE).setVisible(false);
	setDialogStatsVisible(false);
        
        setMapVisible(true);
        
	// AND RESET ALL GAME DATA
	data.reset(this);
	
    }

    /**
     * This mutator method changes the color of the debug text.
     *
     * @param initColor Color to use for rendering debug text.
     */
    public static void setDebugTextColor(Color initColor) {
//        debugTextColor = initColor;
    }

    /**
     * Called each frame, this method updates the rendering state of all
     * relevant GUI controls, like displaying win and loss states and whether
     * certain buttons should be enabled or disabled.
     */
    int backgroundChangeCounter = 0;

    @Override
    public void updateGUI() {
	// IF THE GAME IS OVER, DISPLAY THE APPROPRIATE RESPONSE
	/*
        if (data.won()) {
            System.out.println("Update GUI won");
	    ImageView winImage = guiImages.get(WIN_DISPLAY_TYPE);
	    winImage.setVisible(true);
	    dialogStatsLabel.setVisible(true);
            regionLabel.setVisible(false);
            statsLabel.setVisible(false);
            regionNameLabel.setVisible(false);
            data.endGameAsLoss();
	}
        else if(data.lost()) {
            //System.out.println("Update GUI lost");
            ImageView winImage = guiImages.get(WIN_DISPLAY_TYPE);
	    winImage.setVisible(false);
            dialogStatsLabel.setVisible(false);
            regionLabel.setVisible(false);
            statsLabel.setVisible(true);
            regionNameLabel.setVisible(true);
        }
                */
        
         
    }

    public void reloadMap() {
        // MAKE LABELS IN/VISIBLE
        if(data.lost() || data.won()) {
            setGameLayerVisible(false);
            labelChanges(true, true, true, false, false);
        }

        // RELOAD THE STATUS & NAME & NAVI
        String regionPath = regionDataManager.getCanonicalPathFromRoot(currentRegion);
        regionNameLabel.setText(currentRegion.getName());
        navigationLabel.setText(regionPath);
        
        RegionPlayable regionPlayable = regionDataManager.getPlayable(regionPath);
        String regionScore = "High Score: " + regionPlayable.getHighScore() + "       " +
                             "Fastest Time: " + regionPlayable.getFastestMin() + ":" + regionPlayable.getFastestSec() + "       " +
                             "Fewest Guesses: " + regionPlayable.getFewestGuesses();
        thisRegionScoreLabel.setText(regionScore);
        
        // CHANGE THE MAP VIEW
        Image tempMapImage = loadImage(regionPath + SLASH + currentRegion.getName() + MAP_EXT);
	PixelReader pixelReader = tempMapImage.getPixelReader();
	WritableImage mapImage = new WritableImage(pixelReader, (int) tempMapImage.getWidth(), (int) tempMapImage.getHeight());
	ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setImage(mapImage);
	int numSubRegions = ((RegioVincoDataModel) data).getRegionsFound() + ((RegioVincoDataModel) data).getRegionsNotFound();
	this.boundaryTop = -(numSubRegions * 50);
	
	// MAKE THE OUTER BORDER PIXELS TRANSPARENT
	PixelReader mapReader = mapImage.getPixelReader();
	PixelWriter mapWriter = mapImage.getPixelWriter();
	for (int i = 0; i < mapImage.getWidth(); i++) {
	    for (int j = 0; j < mapImage.getHeight(); j++) {
		Color testColor = mapReader.getColor(i, j);
		if (testColor.equals(MAP_COLOR_KEY)) {
		    mapWriter.setColor(i, j, TRANSPARENT_COLOR);
		}
	    }
	}

	// AND GIVE THE WRITABLE MAP TO THE DATA MODEL
	((RegioVincoDataModel) data).setMapImage(mapImage);
        gameModeButtonsChange();
        
        screenChangeButtons(false, true, true);
    }
    
    private void gameModeButtonsChange() {
        RegionPlayable regionPlayable = 
                regionDataManager.getPlayable(regionDataManager.getCanonicalPathFromRoot(currentRegion));
        setNameGameMode(regionPlayable.getNameMode());
        setCapitalGameMode(regionPlayable.getCapitalMode());
        setLeaderGameMode(regionPlayable.getLeaderMode());
        setFlagGameMode(regionPlayable.getFlagMode());
        setStopGameMode(data.inProgress());
    }
    
    public void setNameGameMode(boolean clickable) {
        Button nameGameButton = guiButtons.get(NAME_GAME_TYPE);
        if(clickable)   nameGameButton.setGraphic(new ImageView(loadImage(NAME_GAME_BUTTON_FILE_PATH)));
        else            nameGameButton.setGraphic(new ImageView(loadImage(NAME_GAME_DISABLED_BUTTON_FILE_PATH)));
    }
    
    public void setCapitalGameMode(boolean clickable) {
        Button capitalGameButton = guiButtons.get(CAPITAL_GAME_TYPE);
        if(clickable)   capitalGameButton.setGraphic(new ImageView(loadImage(CAPITAL_GAME_BUTTON_FILE_PATH)));
        else            capitalGameButton.setGraphic(new ImageView(loadImage(CAPITAL_GAME_DISABLED_BUTTON_FILE_PATH)));
           
    }
    
    public void setLeaderGameMode(boolean clickable) {
        Button leaderGameButton = guiButtons.get(LEADER_GAME_TYPE);
        if(clickable)   leaderGameButton.setGraphic(new ImageView(loadImage(LEADER_GAME_BUTTON_FILE_PATH)));
        else            leaderGameButton.setGraphic(new ImageView(loadImage(LEADER_GAME_DISABLED_BUTTON_FILE_PATH)));
    }
    
    public void setFlagGameMode(boolean clickable) {
        Button flagGameButton = guiButtons.get(FLAG_GAME_TYPE);
        if(clickable)   flagGameButton.setGraphic(new ImageView(loadImage(FLAG_GAME_BUTTON_FILE_PATH)));
        else            flagGameButton.setGraphic(new ImageView(loadImage(FLAG_GAME_DISABLED_BUTTON_FILE_PATH)));
    }
    
    public void setStopGameMode(boolean clickable) {
        Button stopGameButton = guiButtons.get(STOP_GAME_TYPE);
        if(clickable)   stopGameButton.setGraphic(new ImageView(loadImage(STOP_GAME_BUTTON_FILE_PATH)));
        else            stopGameButton.setGraphic(new ImageView(loadImage(STOP_GAME_DISABLED_BUTTON_FILE_PATH)));
    }
    
    public void gameModeButtonsVisible(boolean visible) {
        Button nameGameButton = guiButtons.get(NAME_GAME_TYPE);
        Button capitalGameButton = guiButtons.get(CAPITAL_GAME_TYPE);
        Button leaderGameButton = guiButtons.get(LEADER_GAME_TYPE);
        Button flagGameButton = guiButtons.get(FLAG_GAME_TYPE);
        Button stopGameButton = guiButtons.get(STOP_GAME_TYPE);
        
        nameGameButton.setVisible(visible);
        capitalGameButton.setVisible(visible);
        leaderGameButton.setVisible(visible);
        flagGameButton.setVisible(visible);
        stopGameButton.setVisible(visible);
    }
    
    public void setDialogStatsVisible(boolean visible) {
        guiImages.get(WIN_DISPLAY_TYPE).setVisible(visible);
        guiImages.get(WIN_DISPLAY_FLAG_TYPE).setVisible(visible);
	dialogStatsLabel.setVisible(visible);
        Button closeWinDisplayButton = guiButtons.get(CLOSE_WIN_DISPLAY_TYPE);
        closeWinDisplayButton.setVisible(visible);
    }
    
    public void setGameLayerVisible(boolean visible) {
        gameLayer.setVisible(visible);
    }
    
    public void setSettingLayerVisible(boolean visible) {
        settingLayer.setVisible(visible);
        soundCheckBox.setVisible(visible);
        musicCheckBox.setVisible(visible);
    }
    
    public void setHelpLayerVisible(boolean visible) {
        helpLayer.setVisible(visible);
    }
    
    public void setGameModeScreen() {
        // GAME MODE BUTTONS ALL DISABLED
        setNameGameMode(false);
        setLeaderGameMode(false);
        setCapitalGameMode(false);
        setFlagGameMode(false);
        setStopGameMode(true);
        
        // NAVIGATION & SCORE & SUBREGION SCORE INVISIBLE
        navigationLabel.setVisible(false);
        thisRegionScoreLabel.setVisible(false);
        
        // GAME STATUS & STACK VISIBLE
        statsLabel.setVisible(true);
        gameLayer.setVisible(true);
        
        // SCREEN BUTTONS INVISIBLE
        screenChangeButtons(false, false, false);
        
        //
        regionLabel.setText(currentRegion.getName());
        regionLabel.setVisible(true);
    }
    
    public void stopGame() {
        ConfirmDialog dialog = new ConfirmDialog(window);
        String selection = dialog.showOkCancel("Stop Game", "Do you really want to stop?");
        if(selection == OK) {
            data.endGameAsLoss();
            ((RegioVincoDataModel)data).mapNavigate(this);
        }
        else {
            return;
        }
    }
    
    public boolean getSoundMute() {
        return soundCheckBox.isSelected();
    }
    
    public boolean getMusicMute() {
        return musicCheckBox.isSelected();
    }
}