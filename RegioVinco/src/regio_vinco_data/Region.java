package regio_vinco_data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * This class represents a named region for a geographic application where
 * all regions are hierarchically related. A region should have an id, name,
 * and type, and may optionally have a captial.
 * 
 * @author  Richard McKenna 
 *          Debugging Enterprises
 * @version 1.0
 */
public class Region<T extends Comparable<T>> implements Comparable<Region<T>>
{
    // R,G,B COLOR FOR THIS REGION
    private short red;
    private short green;
    private short blue;
    
    // UNIQUE ID
    private String name;
    
    // CAPTIAL OF THIS REGION. NOTE THAT SOME REGIONS DO NOT HAVE A CAPITAL
    private String capital;
    
    // LEADER OF THE REGION
    private String leader;
    
    // THE PARENT REGION, WITHIN WHICH THIS REGION IS CONTAINED
    private Region parentRegion;
    
    // LIST OF CHILD REGIONS FOR THIS ONE. FOR EXAMPLE, A NATION
    // WOULD LIST STATES HERE
    private ArrayList<Region> subRegions;
    
    /**
     * Constructor that initializes the 4 required fields for any
     * region: its name
     */
    public Region(String initName)
    {
        // INIT THE PROVIDED FIELDS
        name = initName;
        
        // NULL THE MISSING FIELDS
        red = 0;
        green = 0;
        blue = 0;
        capital = null;
        leader = null;
        
        // AND SETUP THE LIST SO WE CAN ADD CHILD REGIONS
        subRegions = new ArrayList();
    }
    
    /**
     * Constructor that initializes the 4 required fields for any
     * region: its r,g,b,name
     */
    public Region(short initRed, short initGreen, short initBlue, String initName)
    {
        // INIT THE PROVIDED FIELDS
        red = initRed;
        green = initGreen;
        blue = initBlue;
        name = initName;
        
        // NULL THE MISSING FIELDS
        capital = null;
        leader = null;
        
        // AND SETUP THE LIST SO WE CAN ADD CHILD REGIONS
        subRegions = new ArrayList();
    }
    
    /**
     * Constructor that initializes the 5 required fields for any
     * region: r,g,b,name,capital
     */
    public Region(short initRed, short initGreen, short initBlue, String initName, String initCapital)
    {
        // LET THE OTHER CONSTRUCTOR DO MOST OF THE SETUP WORK
        this(initRed, initGreen, initBlue, initName);
        
        // WE'LL KEEP THE CAPITAL
        capital = initCapital;
    }
    
    /**
     * Constructor that initializes the 6 required fields for any
     * region: r,g,b,name,leader
     */
    public Region(short initRed, short initGreen, short initBlue, 
            String initName, String initCapital, String initLeader)
    {
        // LET THE OTHER CONSTRUCTOR DO MOST OF THE SETUP WORK
        this(initRed, initGreen, initBlue, initName);
        
        // WE'LL KEEP THE CAPITAL
        capital = initCapital;
        leader = initLeader;
    }

    // ACCESSOR METHODS
    
    public short        getRed()            { return red;           }
    public short        getGreen()          { return green;         }
    public short        getBlue()           { return blue;          }
    
    /**
     * Accessor method for getting this region's name.
     * 
     * @return The name of this region.
     */    
    public String       getName()           { return name;          }

    /**
     * Accessor method for getting this region's parent region.
     * 
     * @return The parent region of this region.
     */    
    public Region       getParentRegion()   { return parentRegion;  }

    /**
     * Accessor method for getting this region's capital.
     * 
     * @return The name of the capital of this region.
     */    
    public String       getCapital()        { return capital;       }
    
    public String       getLeader()         { return leader;        }

    /**
     * Accessor method for getting all of this regions subregions
     * in the form of an Iterator.
     * 
     * @return An Iterator that can traverse sequentially through
     * all of the child regions of this region.
     */
    public Iterator<Region> getSubRegions()
    {
        return subRegions.iterator();
    }

    /**
     * Accessor method for getting the child Region of this one that
     * has an id the same as the subRegionId argument.
     * @return The found region with the same id as the subRegionId argument.
     */
    public Region getSubRegion(String subRegionName)
    {
        // GO THROUGH ALL THE CHILD REGIONS
        Iterator it = subRegions.iterator();
        while (it.hasNext())
        {
            Region subRegion = (Region)it.next();
            
            // DID WE FIND IT?
            if (subRegion.name.equals(subRegionName))
            {
                // YUP, RETURN IT
                return subRegion;
            }
        }
        // NOPE, RETURN NULL
        return null;
    }    
    
    /**
     * This method tests to see if this region is a leaf region (i.e.
     * has no child regions) or not.
     * 
     * @return true if this region has child regions, false otherwise.
     */    
    public boolean hasSubRegions()
    {
        return !subRegions.isEmpty();
    }    
    
    /**
     * This method tests to see if this region has a named capital at
     * the moment. 
     * 
     * @return true if this region has a capital, false otherwise.
     */
    public boolean hasCapital() 
    { 
        return capital != null; 
    }
    
    public boolean hasLeader()
    {
        return leader != null;
    }

    // MUTATOR METHODS
    
    public void setRed(short initRed)
    {
        red = initRed;
    }
    
    public void setGreen(short initGreen)
    {
        green = initGreen;
    }
    
    public void setBlue(short initBlue)
    {
        blue = initBlue;
    }

    /**
     * Mutator method for changing this region's textual name.
     * 
     * @param initName Name to be used for this region's display.
     */
    public void setName(String initName)
    {
        name = initName;
    }

    /**
     * Mutator method for changing this region's named capital.
     * 
     * @param initCapital Capital to be used for this region.
     */
    public void setCapital(String initCapital)
    {
        capital = initCapital;
    }
    
    public void setLeader(String initLeader)
    {
        leader = initLeader;
    }
    
    /**
     * Mutator method for setting the parent region for this region. Note
     * that this is a mutual relationship. The parent knows about the child
     * and vice versa, which makes walking up and down the tree easier.
     * 
     * @param initParentRegion The parent region in relation to this one.
     */
    public void setParentRegion(Region initParentRegion)
    {
        parentRegion = initParentRegion;
    }
    
    // ADDITIONAL SERVICE METHODS
    
    /**
     * Adds another region to be a child region of this one. 
     * 
     * @param subRegionToAdd Region to be added as a sub region. For 
     * example, a nation would be a subregion of a continent.
     */
    public void addSubRegion(Region subRegionToAdd)
    {
        // ADD IT TO OUR DATA STRUCTURE
        subRegions.add(subRegionToAdd);
        
        // AND LET'S KEEP IT SORTED BY NAME
        Collections.sort(subRegions);
    }

    /*
     * Removes the subRegionToRemove from the list of children
     * regions for this one.
     */
    public void removeSubRegion(Region subRegionToRemove)
    {
        // TAKE IT OUT OF OUR DATA STRUCTURE
        subRegions.remove(subRegionToRemove);
    }    

    /**
     * Used for comparing Regions for the purpose of sorting them.
     * 
     * @param region The Region to be compared to this one.
     * 
     * @return 0 if they have the same name, -1 if this Region's
     * name alphabetically precedes it, and 1 if it follows it.
     */
    @Override
    public int compareTo(Region<T> region)
    {
        return name.compareTo(region.name);
    }

    /**
     * Method for testing equivalence of this region with the 
     * regionAsObject argument.
     * 
     * @param regionAsObject The region to test for equivalence
     * with this one.
     * 
     * @return true if they have the same id, false otherwise.
     */
    public boolean equals(Object regionAsObject)
    {
        if (regionAsObject instanceof Region)
        {
            Region region = (Region)regionAsObject;
            return name.equals(region.name);
        }
        return false;
    }

    /**
     * Generates a textual representation of this region.
     * 
     * @return The textual representation of this region, which is
     * simply the name.
     */
   @Override
    public String toString()
    {
        return name;
    }
    
    public int sizeSubRegions() {
        return subRegions.size();
    }
}