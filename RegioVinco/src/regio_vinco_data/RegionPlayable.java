/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regio_vinco_data;

/**
 *
 * @author CHUNGJUNG
 */
public class RegionPlayable {
    private boolean nameMode;
    private boolean capitalMode;
    private boolean leaderMode;
    private boolean flagMode;
    
    private int highScore;
    private int fastestMin;
    private int fastestSec;
    private int fewestGuesses;
    
    public RegionPlayable(boolean initNameMode, boolean initCapitalMode, 
            boolean initLeaderMode, boolean initFlagMode) {
        nameMode = initNameMode;
        capitalMode = initCapitalMode;
        leaderMode = initLeaderMode;
        flagMode = initFlagMode;
        highScore = 0;
        fastestMin = 0;
        fastestSec = 0;
        fewestGuesses = 0;
    }
    
    public boolean getNameMode() {
        return nameMode;
    }
    
    public boolean getCapitalMode() {
        return capitalMode;
    }
    
    public boolean getLeaderMode() {
        return leaderMode;
    }
    
    public boolean getFlagMode() {
        return flagMode;
    }
    
    public int getHighScore() {
        return highScore;
    }
    
    public int getFastestMin() {
        return fastestMin;
    }
    
    public int getFastestSec() {
        return fastestSec;
    }
    
    public int getFewestGuesses() {
        return fewestGuesses;
    }
    
    public void setNameMode(boolean initNameMode) {
        nameMode = initNameMode;
    }
    
    public void setCapitalMode(boolean initCapitalMode) {
        capitalMode = initCapitalMode;
    }
    
    public void setLeaderMode(boolean initLeaderMode) {
        leaderMode = initLeaderMode;
    }
    
    public void setFlagMode(boolean initFlagMode) {
        flagMode = initFlagMode;
    }
    
    public void setHighScore(int initHighScore) {
        highScore = initHighScore;
    }
    
    public void setFastestMin(int initFastestMin) {
        fastestMin = initFastestMin;
    }
    
    public void setFastestSec(int initFastestSec) {
        fastestSec = initFastestSec;
    }
    
    public void setFewestGuesses(int initFewestGuesses) {
        fewestGuesses = initFewestGuesses;
    }
    
    @Override
    public String toString() {
        String result = "";
        result += nameMode + "/" + capitalMode + "/" + leaderMode + "/" + flagMode + "/";
        return result;
    }
}
